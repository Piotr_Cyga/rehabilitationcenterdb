package pl.rp.RehabilitationCenter.Reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.DAO.IKindDAO;
import pl.rp.RehabilitationCenter.Entity.Animal;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ReportsService {

    @Autowired
    private IAnimalDAO animalDAO;

    private static LocalDate YEAR1900 = LocalDate.of(1900,01, 01);

    public Map<Integer, Integer> animalsOfAllKindsAmountInPeriod(LocalDate from, LocalDate to){
        Map<Integer, Integer> reportMap = new HashMap<>();
        for (Animal animal : animalDAO.findAnimalsByTimeArrivedBetween(from, to) ) {
            if (reportMap.containsKey(animal.getKind().getKindId())){
                reportMap.replace(animal.getKind().getKindId(), reportMap.get(animal.getKind().getKindId())+1);
            } else {
            reportMap.put(animal.getKind().getKindId(), 1);
            }
        }
        return reportMap;
    }
}
