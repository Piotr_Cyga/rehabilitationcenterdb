package pl.rp.RehabilitationCenter.Reporting;

import pl.rp.RehabilitationCenter.Entity.Kind;

public class YearToDateReportKindEnumeration {
    private Kind kind;
    private int totalNo;
    private int deceasedNo;
    private int dischargedNo;
    private int releaseNo;

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public int getTotalNo() {
        return totalNo;
    }

    public void setTotalNo(int totalNo) {
        this.totalNo = totalNo;
    }

    public int getDeceasedNo() {
        return deceasedNo;
    }

    public void setDeceasedNo(int deceasedNo) {
        this.deceasedNo = deceasedNo;
    }

    public int getDischargedNo() {
        return dischargedNo;
    }

    public void setDischargedNo(int dischargedNo) {
        this.dischargedNo = dischargedNo;
    }

    public int getReleaseNo() {
        return releaseNo;
    }

    public void setReleaseNo(int releaseNo) {
        this.releaseNo = releaseNo;
    }
}
