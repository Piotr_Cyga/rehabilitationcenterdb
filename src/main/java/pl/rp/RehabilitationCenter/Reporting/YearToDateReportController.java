package pl.rp.RehabilitationCenter.Reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.DAO.IKindDAO;
import pl.rp.RehabilitationCenter.Entity.Kind;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
public class YearToDateReportController {

    @Autowired
    IKindDAO kindDAO;

    @Autowired
    IAnimalDAO animalDAO;

    @RequestMapping ("reports/yearToDateReport")
    public String  yearToDateReport(Model model){
        Map<Integer, YearToDateReportKindEnumeration> list = new HashMap<>();
        AtomicInteger rowCount = new AtomicInteger();
        LocalDate beginOfYear = LocalDate.of(LocalDate.now().getYear(), 01, 01);
        AtomicInteger atomicInteger = new AtomicInteger();
        for (Kind kind : kindDAO.findAll()) {
            if (animalDAO.countAnimalsByKindAndTimeArrivedBetween(kind, beginOfYear, LocalDate.now())>0){
                YearToDateReportKindEnumeration yearToDateReportKindEnumeration = new YearToDateReportKindEnumeration();
                yearToDateReportKindEnumeration.setKind(kind);
                yearToDateReportKindEnumeration.setTotalNo(animalDAO.countAnimalsByKindAndTimeArrivedBetween(kind, beginOfYear, LocalDate.now()));
                yearToDateReportKindEnumeration.setReleaseNo(animalDAO.countAnimalsByKindAndIfReleasedIsTrueAndTimeArrivedBetween(kind, beginOfYear, LocalDate.now()));
                yearToDateReportKindEnumeration.setDeceasedNo(animalDAO.countAnimalsByKindAndIfDeceasedIsTrueAndTimeArrivedBetween(kind, beginOfYear, LocalDate.now()));
                yearToDateReportKindEnumeration.setDischargedNo(animalDAO.countAnimalsByKindAndDischargedToIsNotNullAndTimeArrivedBetween(kind, beginOfYear, LocalDate.now()));
                list.put(atomicInteger.incrementAndGet(), yearToDateReportKindEnumeration);
            }
        }
        model.addAttribute("list", list);
        return "reports/yearToDateReport";
    }
}
