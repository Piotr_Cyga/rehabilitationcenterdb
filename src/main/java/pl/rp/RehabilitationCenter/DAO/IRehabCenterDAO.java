package pl.rp.RehabilitationCenter.DAO;

import org.springframework.data.repository.CrudRepository;
import pl.rp.RehabilitationCenter.Entity.RehabCenter;

public interface IRehabCenterDAO extends CrudRepository<RehabCenter, Integer> {
}
