package pl.rp.RehabilitationCenter.DAO;

import org.springframework.data.repository.CrudRepository;
import pl.rp.RehabilitationCenter.Entity.Location;

public interface ILocationDAO extends CrudRepository<Location, Integer> {
    Location findByName(String name);
}
