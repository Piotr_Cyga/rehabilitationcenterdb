package pl.rp.RehabilitationCenter.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.rp.RehabilitationCenter.Entity.Kind;

import java.util.List;

@Repository
public interface IKindDAO extends CrudRepository<Kind, Integer> {
}
