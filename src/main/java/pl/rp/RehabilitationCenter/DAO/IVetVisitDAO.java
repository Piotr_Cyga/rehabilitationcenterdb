package pl.rp.RehabilitationCenter.DAO;

import org.springframework.data.repository.CrudRepository;
import pl.rp.RehabilitationCenter.Entity.VetVisit;

import java.util.List;

public interface IVetVisitDAO extends CrudRepository<VetVisit, Integer> {
    List<VetVisit> getVetVisitsByAnimal_AnimalId(Integer animalId);
}
