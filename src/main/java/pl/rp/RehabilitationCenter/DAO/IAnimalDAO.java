package pl.rp.RehabilitationCenter.DAO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.rp.RehabilitationCenter.Entity.Animal;
import pl.rp.RehabilitationCenter.Entity.Kind;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

public interface IAnimalDAO extends PagingAndSortingRepository<Animal, Integer> {

    Page<Animal> findByIfReleasedFalseAndIfDeceasedFalseAndDischargedToNullOrderByAnimalIdDesc(Pageable page);
    Animal findAnimalByAnimalId(Integer animalId);
    Page<Animal> findAllByOrderByAnimalIdDesc(Pageable page);
    List<Animal> findAnimalsByTimeArrivedBetween(LocalDate from, LocalDate to);
    int countAnimalsByKindAndTimeArrivedBetween(Kind kind, LocalDate from, LocalDate to);
    int countAnimalsByKindAndIfDeceasedIsTrueAndTimeArrivedBetween (Kind kind, LocalDate from, LocalDate to);
    int countAnimalsByKindAndIfReleasedIsTrueAndTimeArrivedBetween (Kind kind, LocalDate from, LocalDate to);
    int countAnimalsByKindAndDischargedToIsNotNullAndTimeArrivedBetween (Kind kind, LocalDate from, LocalDate to);
    int countAnimalsByKindAndIfReleasedIsFalseAndIfDeceasedIsFalseAndDischargedToIsNullAndTimeArrivedBetween (Kind kind, LocalDate from, LocalDate to);
}
