package pl.rp.RehabilitationCenter.DAO;

import org.springframework.data.repository.CrudRepository;
import pl.rp.RehabilitationCenter.Entity.Veterinarian;

import java.util.List;

public interface IVeterinarianDAO extends CrudRepository<Veterinarian, Integer> {
}
