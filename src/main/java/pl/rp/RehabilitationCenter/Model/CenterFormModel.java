package pl.rp.RehabilitationCenter.Model;

public class CenterFormModel {
    private Integer rehabCenterId;

    private String name;

    private String fullData;

    public Integer getRehabCenterId() {
        return rehabCenterId;
    }

    public void setRehabCenterId(Integer rehabCenterId) {
        this.rehabCenterId = rehabCenterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullData() {
        return fullData;
    }

    public void setFullData(String fullData) {
        this.fullData = fullData;
    }
}
