package pl.rp.RehabilitationCenter.Model;

import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.Entity.Animal;
import pl.rp.RehabilitationCenter.Entity.Kind;
import pl.rp.RehabilitationCenter.Entity.Veterinarian;
import pl.rp.RehabilitationCenter.Service.PredefinedTime;

import java.time.LocalDate;

public class VetVisitFormModel {
    private Integer visitId;
    private Animal animal;
    private Kind kind;
    private Integer vetId;
    private String date;
    private String note;
    private PredefinedTime predefinedTime;
    private Float price;

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public PredefinedTime getPredefinedTime() {
        return predefinedTime;
    }

    public void setPredefinedTime(PredefinedTime predefinedTime) {
        this.predefinedTime = predefinedTime;
    }

    public Integer getVisitId() {
        return visitId;
    }

    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public Integer getVetId() {
        return vetId;
    }

    public void setVetId(Integer vetId) {
        this.vetId = vetId;
    }

    public LocalDate getDate() {
        return LocalDate.parse(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
