package pl.rp.RehabilitationCenter.Model;

import pl.rp.RehabilitationCenter.Entity.Kind;
import pl.rp.RehabilitationCenter.Entity.Location;
import pl.rp.RehabilitationCenter.Entity.RehabCenter;

import java.time.LocalDate;

public class ModifyAnimalDataFormModel {
    private Kind kind;
    private boolean ifAdult;
    private LocalDate timeArrived;
    private LocalDate timeDischarged;
    private boolean ifReleased;
    private boolean ifDeceased;
    private RehabCenter dischargedTo;
    private Location location;
    private String additionalInfo;

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public boolean isIfAdult() {
        return ifAdult;
    }

    public void setIfAdult(boolean ifAdult) {
        this.ifAdult = ifAdult;
    }

    public LocalDate getTimeArrived() {
        return timeArrived;
    }

    public void setTimeArrived(LocalDate timeArrived) {
        this.timeArrived = timeArrived;
    }

    public LocalDate getTimeDischarged() {
        return timeDischarged;
    }

    public void setTimeDischarged(LocalDate timeDischarged) {
        this.timeDischarged = timeDischarged;
    }

    public boolean isIfReleased() {
        return ifReleased;
    }

    public void setIfReleased(boolean ifReleased) {
        this.ifReleased = ifReleased;
    }

    public boolean isIfDeceased() {
        return ifDeceased;
    }

    public void setIfDeceased(boolean ifDeceased) {
        this.ifDeceased = ifDeceased;
    }

    public RehabCenter getDischargedTo() {
        return dischargedTo;
    }

    public void setDischargedTo(RehabCenter dischargedTo) {
        this.dischargedTo = dischargedTo;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
