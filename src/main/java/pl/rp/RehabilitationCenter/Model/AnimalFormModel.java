package pl.rp.RehabilitationCenter.Model;

import pl.rp.RehabilitationCenter.Entity.Kind;
import pl.rp.RehabilitationCenter.Entity.Location;
import pl.rp.RehabilitationCenter.Entity.RehabCenter;
import pl.rp.RehabilitationCenter.Service.PredefinedTime;

import java.time.LocalDate;
import java.util.Date;

public class AnimalFormModel {

    private PredefinedTime predefinedTime;

    private RehabCenter dischargedTo;

    private Integer kindId;

    private boolean ifAdult;

    private String additionalInfo;

    private Location location;

    private Integer locationId;

    private Kind kind;

    private Integer animalId;

    private LocalDate timeArrived;

    private Date timeArrivedForModify;

    private LocalDate timeDischarged;

    private String date;

    public LocalDate getDate() {
        return LocalDate.parse(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Integer animalId) {
        this.animalId = animalId;
    }

    public LocalDate getTimeDischarged() {
        return timeDischarged;
    }

    public void setTimeDischarged(LocalDate timeDischarged) {
        this.timeDischarged = timeDischarged;
    }

    public PredefinedTime getPredefinedTime() {
        return predefinedTime;
    }

    public void setPredefinedTime(PredefinedTime predefinedTime) {
        this.predefinedTime = predefinedTime;
    }

    public void setTimeArrived(LocalDate timeArrived) {
        this.timeArrived = timeArrived;
    }

    public LocalDate getTimeArrived() {
        return timeArrived;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public boolean isIfAdult() {
        return ifAdult;
    }

    public void setIfAdult(boolean ifAdult) {
        this.ifAdult = ifAdult;
    }

    public void setKindId(Integer kindId) {
        this.kindId = kindId;
    }

    public Integer getKindId() {
        return kindId;
    }

    public RehabCenter getDischargedTo() {
        return dischargedTo;
    }

    public void setDischargedTo(RehabCenter dischargedTo) {
        this.dischargedTo = dischargedTo;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Date getTimeArrivedForModify() {
        return timeArrivedForModify;
    }

    public void setTimeArrivedForModify(Date timeArrivedForModify) {
        this.timeArrivedForModify = timeArrivedForModify;
    }
}