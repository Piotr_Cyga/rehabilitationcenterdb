package pl.rp.RehabilitationCenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.rp.RehabilitationCenter.Service.KindDAOService;
import pl.rp.RehabilitationCenter.Service.KindGroup;

@SpringBootApplication
public class RehabilitationCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RehabilitationCenterApplication.class, args);
		//just to check if working//
//		KindDAOService kindDAOService = new KindDAOService();
//		kindDAOService.importListOfAnimals(KindGroup.BIRD);
	}
}
