package pl.rp.RehabilitationCenter.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.stereotype.Repository;
import pl.rp.RehabilitationCenter.DAO.IKindDAO;
import pl.rp.RehabilitationCenter.Entity.Kind;

import java.io.*;
import java.util.Scanner;
@Repository
public class KindDAOService {
    @Autowired
    IKindDAO kindDAO;



    public void importListOfAnimals(KindGroup kindGroup) {
        String path = "";
        int rowCount = 0;
        switch (kindGroup) {
            case BIRD:
                path= System.getProperty("user.dir") + "/src/main/resources/birdKindList";
                break;
            case MAMMAL:
                path= System.getProperty("user.dir") + "/src/main/resources/mammalKindList";
                break;
        }
        try {
            Scanner scanner = new Scanner(new File(path), "Utf-8");
            scanner.useDelimiter(",|\\n");
            Kind kind = null;
            while (scanner.hasNext()) {
                    if (rowCount==0) {
                        kind = new Kind();
                        kind.setKindPolish(scanner.next());
                        rowCount = 1;
                    }
                    else if (rowCount==1) {
                        kind.setKindLatin(scanner.next());
                        rowCount=0;
                        kind.setKindGroup(kindGroup);
                        kindDAO.save(kind);
                    }
                }
                scanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//
//        try {
//            Scanner scanner = new Scanner(new File(path));
//            scanner.useDelimiter(",");
//            Kind kind = new Kind();
//            while (scanner.hasNext()) {
//                for (int i = 0; i < 2; ) {
//                    switch (i) {
//                        case 0:
//                            if (scanner.hasNext()) {
//                                String kindPolish = scanner.next();
//                                kind.setKindPolish(kindPolish);
//                                System.out.println(kindPolish + i);
//                            }
//                            i++;
//                            break;
//                        case 1:
//                            if (scanner.hasNext()) {
//                                String kindLatin = scanner.next();
//
//                                kind.setKindLatin(kindLatin);
//                                System.out.println(kindLatin + i);
//                            }
//                            i++;
//                            break;
//
//                        default:
//                            i = 2;
//                            break;
//                    }
//                    kind.setKindGroup(kindGroup);
//                    kindDAO.save(kind);
//                }
//            }
//                scanner.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        try {
//            String allInput =new String(Files.readAllBytes(Paths.get(path)));
//            String [] splittedInput = allInput.split(",");
//            for (int i=1; i < splittedInput.length; i++ ) {
//                Kind kind = new Kind();
//                for (int j=0; j<2; j++) {
//                    switch (i % 2) {
//                        case 0:
//                            kind.setKindPolish(splittedInput[i]);
//                            i++;
//                            break;
//                        case 1:
//                            kind.setKindLatin(splittedInput[i]);
//                            i++;
//                            break;
//                    }
//                }
//                kind.setKindGroup(kindGroup);
//                kindDAO.save(kind);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
}
