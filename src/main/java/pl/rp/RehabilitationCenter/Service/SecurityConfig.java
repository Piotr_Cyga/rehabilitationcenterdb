package pl.rp.RehabilitationCenter.Service;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("cygusblues").password("1986.lutra").roles("admin");
        auth.inMemoryAuthentication().withUser("ratujmy").password("ptaki").roles("admin");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginProcessingUrl("/login")
                .loginPage("/login_form")
                .defaultSuccessUrl("/manage")
                .failureUrl("/access_denied")
                .usernameParameter("username")
                .passwordParameter("password")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/ratujmy_ptaki")
                .and()
                .authorizeRequests()
                .antMatchers("/ratujmy_ptaki").permitAll()
                .antMatchers("/img/*").permitAll()
                .antMatchers("/manage").permitAll()
                .antMatchers("/add_center").authenticated()
                .antMatchers("/add_center/**/*").authenticated()
                .antMatchers("/add_vet").authenticated()
                .antMatchers("/add_visit_by_animal/*").authenticated()
                .antMatchers("/addLocation").authenticated()
                .antMatchers("/deceased/*").authenticated()
                .antMatchers("/discharged_to/*").authenticated()
                .antMatchers("/importAll").authenticated()
                .antMatchers("/modifyAnimal/*").authenticated()
                .antMatchers("/list_animals").permitAll()
                .antMatchers("/new_animal").authenticated()
                .antMatchers("/rehab_center_list").permitAll()
                .antMatchers("/release/*").authenticated()
                .antMatchers("/vet_list").permitAll()
                .antMatchers("/vet_visits").permitAll()
                .antMatchers("/view_mode").permitAll()
                .antMatchers("/vet_visits_by_animal/*").permitAll();
    }
}
