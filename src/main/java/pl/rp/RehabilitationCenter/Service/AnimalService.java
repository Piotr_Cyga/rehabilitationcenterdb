package pl.rp.RehabilitationCenter.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rp.RehabilitationCenter.DAO.IKindDAO;
import pl.rp.RehabilitationCenter.Entity.Animal;
import pl.rp.RehabilitationCenter.Entity.Kind;

import java.util.LinkedList;
import java.util.List;

@Service
public class AnimalService {

    @Autowired
    private IKindDAO kindDAO;

    public List<Kind> getKindsWithAnimalKindFirst(Animal animal) {
        List<Kind> allKindsCurrentFirst = new LinkedList<>();
        kindDAO.findAll().forEach(kind -> {
            if (!kind.equals(animal.getKind())) {
                allKindsCurrentFirst.add(kind);
            } else allKindsCurrentFirst.add(0, kind);
        });
        return allKindsCurrentFirst;
    }
}
