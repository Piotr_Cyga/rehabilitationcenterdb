package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rp.RehabilitationCenter.DAO.IVeterinarianDAO;
import pl.rp.RehabilitationCenter.Entity.Veterinarian;
import pl.rp.RehabilitationCenter.Model.VetFormModel;

@Controller
public class VetListController {

    @Autowired
    IVeterinarianDAO veterinarianDAO;

    @GetMapping("vet_list")
    public String listVets (Model model){


        model.addAttribute("list", veterinarianDAO.findAll());
        return ("vet_list");
    }

    @GetMapping("add_vet")
    public String addVet (Model model){
        VetFormModel formModel = new VetFormModel();
        model.addAttribute("newVet", formModel);
        return "add_vet";
    }
    @PostMapping("add_vet")
    public String addVet (VetFormModel formModel){
        Veterinarian newVet = new Veterinarian();
        newVet.setName(formModel.getName());
        newVet.setFullData(formModel.getAdress());
        veterinarianDAO.save(newVet);
        return "redirect:/vet_list";
    }
}
