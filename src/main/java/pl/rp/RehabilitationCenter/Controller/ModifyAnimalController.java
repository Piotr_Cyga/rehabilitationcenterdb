package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.DAO.IKindDAO;
import pl.rp.RehabilitationCenter.DAO.ILocationDAO;
import pl.rp.RehabilitationCenter.Entity.Animal;
import pl.rp.RehabilitationCenter.Entity.Location;
import pl.rp.RehabilitationCenter.Model.AnimalFormModel;
import pl.rp.RehabilitationCenter.Service.AnimalService;

@Controller
public class ModifyAnimalController {

    @Autowired
    IAnimalDAO animalDAO;
    @Autowired
    IKindDAO kindDAO;
    @Autowired
    ILocationDAO locationDAO;
    @Autowired
    AnimalService animalService;

    @GetMapping("modifyAnimal/{animalId}")
    public String modifyAnimalDataById(Model model, @PathVariable("animalId") Integer animalId) {
        AnimalFormModel animalData = new AnimalFormModel();
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);

        animalData.setKind(animal.getKind());
        animalData.setLocation(animal.getLocation());
        animalData.setDate(animal.getTimeArrived().toString());
        animalData.setIfAdult(animal.isIfAdult());
        animalData.setAdditionalInfo(animal.getAdditionalInfo());

        model.addAttribute("animal", animal);
        model.addAttribute("animalId", animalId);
        model.addAttribute("form", animalData);
        model.addAttribute("kinds", animalService.getKindsWithAnimalKindFirst(animal));


        return "modifyAnimal";
    }


    @PostMapping("modifyAnimal/{animalId}")
    public String modifyAnimalData(AnimalFormModel animalFormModel, @PathVariable("animalId") Integer animalId) {
        Animal animal = animalDAO.findOne(animalId);
        animal.setIfAdult(animalFormModel.isIfAdult());
        animal.setDischargedTo(animalFormModel.getDischargedTo());

        if (null != animalFormModel.getKindId()) {
            animal.setKind(kindDAO.findOne(animalFormModel.getKindId()));
        }

        if ((!animalFormModel.getAdditionalInfo().isEmpty()) && (!animalFormModel.getAdditionalInfo().equalsIgnoreCase(animal.getAdditionalInfo()))) {
            animal.setAdditionalInfo(animalFormModel.getAdditionalInfo());
        } else {
            animal.setAdditionalInfo(animalFormModel.getAdditionalInfo());
        }

        if (null != animalFormModel.getDate()) {
            animal.setTimeArrived(animalFormModel.getDate());
        }
        Location location;
        if (locationDAO.findByName(animalFormModel.getLocation().getName().toLowerCase()) == null) {
            location = new Location();
            location.setName(animalFormModel.getLocation().getName().toLowerCase());
            animal.setLocation(location);
            locationDAO.save(location);
        } else {
            location = locationDAO.findByName(animalFormModel.getLocation().getName().toLowerCase());
            animal.setLocation(location);
        }
        animalDAO.save(animal);


        return "redirect:/list_animals";
    }
}
