package pl.rp.RehabilitationCenter.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.concurrent.TimeUnit;

@Controller
public class MainController {
    @GetMapping ("/")
    public String main () {
        return "redirect:/ratujmy_ptaki";
    }

    @GetMapping ("/ratujmy_ptaki")
    public String ratujmyPtaki() {
        return "ratujmy_ptaki";
    }
@GetMapping ("/manage")
    public String manage() {
        return "manage";
}
}
