package pl.rp.RehabilitationCenter.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

  @GetMapping("login_form")
  public String loginForm() {
    return "login_form";
  }

  @GetMapping("access_denied")
  public String access_denied() {
    return "access_denied";
  }

  @GetMapping("view_mode")
  public String viewMode() {
    return "view_mode";
  }
}
