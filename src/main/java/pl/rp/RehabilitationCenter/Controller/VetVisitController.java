package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.DAO.IVetVisitDAO;
import pl.rp.RehabilitationCenter.DAO.IVeterinarianDAO;
import pl.rp.RehabilitationCenter.Entity.Animal;
import pl.rp.RehabilitationCenter.Entity.VetVisit;
import pl.rp.RehabilitationCenter.Entity.Veterinarian;
import pl.rp.RehabilitationCenter.Model.AnimalSearchFormModel;
import pl.rp.RehabilitationCenter.Model.VetVisitFormModel;
import pl.rp.RehabilitationCenter.Service.PredefinedTime;

import java.time.LocalDate;

@Controller
public class VetVisitController {

    @Autowired
    IVetVisitDAO vetVisitDAO;

    @Autowired
    IVeterinarianDAO veterinarianDAO;

    @Autowired
    IAnimalDAO animalDAO;

    @RequestMapping("vet_visits")
    public String listAllVisits(Model model) {
        model.addAttribute("visitList", vetVisitDAO.findAll());
        return "vet_visits";
    }

    @GetMapping("vet_visits_by_animal/{animalId}")
    public String listVisitsByAnimal (Model model, @PathVariable("animalId") Integer animalId) {
        model.addAttribute("visitList", vetVisitDAO.getVetVisitsByAnimal_AnimalId(animalId));
        return "vet_visits";
    }

    @GetMapping("add_visit_by_animal/{animalId}")
    public String addVisitsByAnimal (Model model, @PathVariable("animalId") Integer animalId) {
        VetVisitFormModel newVisit=new VetVisitFormModel();
        model.addAttribute("newVisit", newVisit);
        model.addAttribute("list", veterinarianDAO.findAll());
        model.addAttribute("animal", animalDAO.findOne(animalId));
        model.addAttribute("animalId", animalId);
        return "add_visit_by_animal";
    }
    @PostMapping ("add_visit_by_animal/{animalId}")
    public String saveVisitByAnimal (VetVisitFormModel vetVisitFormModel, @PathVariable("animalId") Integer animalId) {
        VetVisit vetVisit = new VetVisit();
        vetVisit.setAnimal(animalDAO.findAnimalByAnimalId(animalId));
        vetVisit.setNotes(vetVisitFormModel.getNote());
        vetVisit.setPrice(vetVisitFormModel.getPrice());
        vetVisit.setVet(veterinarianDAO.findOne(vetVisitFormModel.getVetId()));
        vetVisit.setDate(vetVisitFormModel.getDate());
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);
        animal.getVetVisits().add(vetVisit);
        animalDAO.save(animal);
        vetVisitDAO.save(vetVisit);
        return "redirect:/vet_visits_by_animal/"+animalId;
    }






}
