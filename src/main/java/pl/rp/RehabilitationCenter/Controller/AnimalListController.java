package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.Model.AnimalSearchFormModel;
import pl.rp.RehabilitationCenter.Service.SearchService;

@Controller
public class AnimalListController {

    @Autowired
    IAnimalDAO animalDAO;

@Autowired
private SearchService searchService;


    @RequestMapping ("list_animals")
    public String listAllAnimals (Model model, AnimalSearchFormModel searchFormModel, @PageableDefault(size = 25) org.springframework.data.domain.Pageable page) {
        if (searchFormModel.getCurrent() != null) {
            searchService.setCurrent(searchFormModel.getCurrent());
        }

        if (Boolean.FALSE.equals(searchService.isCurrent())) {
            model.addAttribute("list", animalDAO.findAllByOrderByAnimalIdDesc(page));
            model.addAttribute("current", false);
        } else {
            model.addAttribute("list", animalDAO.findByIfReleasedFalseAndIfDeceasedFalseAndDischargedToNullOrderByAnimalIdDesc(page));
            model.addAttribute("current", true);
        }

        searchFormModel.setCurrent(searchService.isCurrent());
        model.addAttribute("searchFormModel", searchFormModel);

        return "list_animals";
    }

    @GetMapping("/update/{animalId}")
    public String discharge(Model model, @PathVariable("animalId") int animalId){
    model.addAttribute("animal", animalDAO.findAnimalByAnimalId(animalId));
    return "update";
    }
}
