package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.rp.RehabilitationCenter.DAO.IRehabCenterDAO;
import pl.rp.RehabilitationCenter.Entity.RehabCenter;
import pl.rp.RehabilitationCenter.Entity.Veterinarian;
import pl.rp.RehabilitationCenter.Model.CenterFormModel;
import pl.rp.RehabilitationCenter.Model.VetFormModel;

@Controller
public class RehabCenterController {
    @Autowired
    IRehabCenterDAO rehabCenterDAO;

    @GetMapping ("rehab_center_list")
    public String listAllCenters (Model model){
        model.addAttribute("list", rehabCenterDAO.findAll());
        return ("rehab_center_list");
    }
    @GetMapping("add_center")
    public String addCenter (Model model){
        CenterFormModel formModel = new CenterFormModel();
        model.addAttribute("newCenter", formModel);
        return "add_center";
    }
    @PostMapping("add_center")
    public String addCenter (CenterFormModel formModel){
        RehabCenter newCenter = new RehabCenter();
        newCenter.setName(formModel.getName());
        newCenter.setFullData(formModel.getFullData());
        rehabCenterDAO.save(newCenter);
        return "redirect:/rehab_center_list";
    }
}
