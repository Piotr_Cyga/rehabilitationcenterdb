package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import pl.rp.RehabilitationCenter.DAO.IKindDAO;
import pl.rp.RehabilitationCenter.Service.KindDAOService;
import pl.rp.RehabilitationCenter.Service.KindGroup;

@Controller
public class SettingsController {
@Autowired
KindDAOService kindDAOService;

    @GetMapping ("/importAll")
    public String importKinds(){
        kindDAOService.importListOfAnimals(KindGroup.MAMMAL);
        kindDAOService.importListOfAnimals(KindGroup.BIRD);
        return "redirect:/manage";
    }
}
