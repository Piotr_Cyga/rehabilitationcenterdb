package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.DAO.IRehabCenterDAO;
import pl.rp.RehabilitationCenter.Entity.Animal;
import pl.rp.RehabilitationCenter.Model.AnimalFormModel;
import pl.rp.RehabilitationCenter.Service.PredefinedTime;

import java.time.LocalDate;

@Controller
public class AnimalChangeController {
    @Autowired
    IAnimalDAO animalDAO;
    @Autowired
    IRehabCenterDAO rehabCenterDAO;

    @GetMapping("/release/{animalId}")
    public String releaseForm(Model model, @PathVariable("animalId") Integer animalId) {
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);
        AnimalFormModel formModel = new AnimalFormModel();
        formModel.setKind(animal.getKind());
        formModel.setTimeArrived(animal.getTimeArrived());
        formModel.setAnimalId(animal.getAnimalId());

        model.addAttribute("animal", formModel);
        return "release";
    }

    @PostMapping("/release/{animalId}")
    public String updateRelease(AnimalFormModel animalFormModel, @PathVariable ("animalId") Integer animalId) {
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);
        animal.setAdditionalInfo(appendAdditionalInfo(animal, animalFormModel.getAdditionalInfo()));
//        setDate(animalFormModel, animal);
        animal.setTimeDischarged(animalFormModel.getDate());
        animal.setIfReleased(true);
        animalDAO.save(animal);

        return "redirect:/list_animals";
    }

    @GetMapping("/deceased/{animalId}")
    public String deceaseForm(Model model, @PathVariable("animalId") Integer animalId) {
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);
        AnimalFormModel formModel = new AnimalFormModel();
        formModel.setKind(animal.getKind());
        formModel.setTimeArrived(animal.getTimeArrived());
        formModel.setAnimalId(animal.getAnimalId());

        model.addAttribute("animal", formModel);
        return "deceased";
    }

    @PostMapping("/deceased/{animalId}")
    public String updateDecease(AnimalFormModel animalFormModel, @PathVariable ("animalId") Integer animalId) {
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);
        animal.setAdditionalInfo(appendAdditionalInfo(animal, animalFormModel.getAdditionalInfo()));
//        setDate(animalFormModel, animal);
        animal.setTimeDischarged(animalFormModel.getDate());
        animal.setIfDeceased(true);
        animalDAO.save(animal);

        return "redirect:/list_animals";
    }

    private void setDate(AnimalFormModel animalFormModel, Animal animal) {
        if (animalFormModel.getPredefinedTime().equals(PredefinedTime.TODAY)) {
            animal.setTimeDischarged(LocalDate.now());
        }
        else if (animalFormModel.getPredefinedTime().equals(PredefinedTime.YESTERDAY)){
            animal.setTimeDischarged(LocalDate.now().minusDays(1L));
        }
        else animal.setTimeDischarged(LocalDate.now().minusDays(2L));
    }

    @GetMapping("/discharged_to/{animalId}")
    public String dischargedToForm(Model model, @PathVariable("animalId") Integer animalId) {
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);
        AnimalFormModel formModel = new AnimalFormModel();
        formModel.setKind(animal.getKind());
        formModel.setTimeArrived(animal.getTimeArrived());
        formModel.setAnimalId(animal.getAnimalId());
        model.addAttribute("rehabCenters", rehabCenterDAO.findAll());
        model.addAttribute("animal", formModel);
        return "discharged_to";
    }

    @PostMapping("/discharged_to/{animalId}")
    public String updateDischargedTo(AnimalFormModel animalFormModel, @PathVariable ("animalId") Integer animalId) {
        Animal animal = animalDAO.findAnimalByAnimalId(animalId);
        animal.setAdditionalInfo(appendAdditionalInfo(animal, animalFormModel.getAdditionalInfo()));
//        setDate(animalFormModel, animal);
        animal.setTimeDischarged(animalFormModel.getDate());
        animal.setDischargedTo(animalFormModel.getDischargedTo());
        animalDAO.save(animal);

        return "redirect:/list_animals";
    }

    private String appendAdditionalInfo (Animal animal, String newInformation) {
        try {
            String previousInformation = animal.getAdditionalInfo();
            if (previousInformation.isEmpty()) {
                if (newInformation.isEmpty()){
                    return "";
                } else
                return "wypis: " + newInformation;
            } else
                if (newInformation.isEmpty()){
                return previousInformation;
                } else
                return previousInformation + "; wypis: " + newInformation;
        } catch (NullPointerException e) {
            e.printStackTrace();
            if (newInformation.isEmpty()){
                return "";
            } else
                return "wypis: " + newInformation;
        }
    }
//@RequestMapping ("/update/{animalId}")
//    public String chooseAction (Model model, @PathVariable("animalId") Integer animalId) {
//    model.addAttribute("updatedAnimal", animalDAO.findAnimalByAnimalId(animalId));
//return ""
//}
}
