package pl.rp.RehabilitationCenter.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rp.RehabilitationCenter.DAO.IAnimalDAO;
import pl.rp.RehabilitationCenter.DAO.IKindDAO;
import pl.rp.RehabilitationCenter.DAO.ILocationDAO;
import pl.rp.RehabilitationCenter.Entity.Animal;
import pl.rp.RehabilitationCenter.Entity.Location;
import pl.rp.RehabilitationCenter.Model.AnimalFormModel;

@Controller
@RequestMapping("/new_animal")
public class AnimalAddController {

    @Autowired
    IAnimalDAO animalDAO;

    @Autowired
    IKindDAO kindDAO;

    @Autowired
    ILocationDAO locationDAO;

    @GetMapping
    public String addAnimal(Model model) {


        AnimalFormModel newAnimal = new AnimalFormModel();

        model.addAttribute("newAnimal", newAnimal);
        model.addAttribute("kinds", kindDAO.findAll());
        model.addAttribute("locations", locationDAO.findAll());
        return "new_animal";
    }

    @PostMapping
    public String saveNewAnimal(AnimalFormModel animalFormModel) {

        Animal newAnimal = new Animal();
        newAnimal.setIfAdult(animalFormModel.isIfAdult());
        newAnimal.setDischargedTo(animalFormModel.getDischargedTo());
        newAnimal.setKind(kindDAO.findOne(animalFormModel.getKindId()));

        if (!animalFormModel.getAdditionalInfo().isEmpty()) {
            newAnimal.setAdditionalInfo("przyjęcie: " + animalFormModel.getAdditionalInfo());
        } else {
            newAnimal.setAdditionalInfo(animalFormModel.getAdditionalInfo());
        }

        newAnimal.setTimeArrived(animalFormModel.getDate());
        Location location;
        if (locationDAO.findByName(animalFormModel.getLocation().getName().toLowerCase()) == null) {
            location = new Location();
            location.setName(animalFormModel.getLocation().getName().toLowerCase());
            newAnimal.setLocation(location);
            locationDAO.save(location);
        } else {
            location = locationDAO.findByName(animalFormModel.getLocation().getName().toLowerCase());
            newAnimal.setLocation(location);
        }
        animalDAO.save(newAnimal);


        return "redirect:/list_animals";
    }
}
