package pl.rp.RehabilitationCenter.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RehabCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rehabCenterId;

    private String name;

    private String fullData;

    public Integer getRehabCenterId() {
        return rehabCenterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullData() {
        return fullData;
    }

    public void setFullData(String fullData) {
        this.fullData = fullData;
    }
}
