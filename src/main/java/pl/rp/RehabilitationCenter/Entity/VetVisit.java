package pl.rp.RehabilitationCenter.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class VetVisit {
    @Id@GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer visitId;

    @ManyToOne
    private Animal animal;

    @ManyToOne
    private Veterinarian vet;

    private LocalDate date;

    private Float price;

    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getVisitId() {
        return visitId;
    }

    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Veterinarian getVet() {
        return vet;
    }

    public void setVet(Veterinarian vet) {
        this.vet = vet;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
