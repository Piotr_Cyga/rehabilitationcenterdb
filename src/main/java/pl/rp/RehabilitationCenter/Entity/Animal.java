package pl.rp.RehabilitationCenter.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer animalId;

    @ManyToOne
    private Kind kind;

    private boolean ifAdult;

    private LocalDate timeArrived;

    private LocalDate timeDischarged;

    private boolean ifReleased;

    private boolean ifDeceased;

    @ManyToOne
    private RehabCenter dischargedTo;

    @OneToMany (mappedBy = "animal")
    private List<VetVisit> vetVisits;

    @ManyToOne
    private Location location;

    private String additionalInfo;

    public RehabCenter getDischargedTo() {
        return dischargedTo;
    }

    public void setDischargedTo(RehabCenter dischargedTo) {
        this.dischargedTo = dischargedTo;
    }

    public Integer getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Integer animalId) {
        this.animalId = animalId;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public boolean isIfAdult() {
        return ifAdult;
    }

    public void setIfAdult(boolean ifAdult) {
        this.ifAdult = ifAdult;
    }

    public LocalDate getTimeArrived() {
        return timeArrived;
    }

    public void setTimeArrived(LocalDate timeArrived) {
        this.timeArrived = timeArrived;
    }

    public LocalDate getTimeDischarged() {
        return timeDischarged;
    }

    public void setTimeDischarged(LocalDate timeDischarged) {
        this.timeDischarged = timeDischarged;
    }

    public boolean isIfReleased() {
        return ifReleased;
    }

    public void setIfReleased(boolean ifReleased) {
        this.ifReleased = ifReleased;
    }

    public boolean isIfDeceased() {
        return ifDeceased;
    }

    public void setIfDeceased(boolean ifDeceased) {
        this.ifDeceased = ifDeceased;
    }

    public List<VetVisit> getVetVisits() {
        return vetVisits;
    }

    public void setVetVisits(List<VetVisit> vetVisits) {
        this.vetVisits = vetVisits;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
