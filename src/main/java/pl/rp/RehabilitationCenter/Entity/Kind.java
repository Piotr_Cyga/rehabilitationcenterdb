package pl.rp.RehabilitationCenter.Entity;

import pl.rp.RehabilitationCenter.Service.KindGroup;

import javax.persistence.*;

@Entity
public class Kind {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer kindId;
    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    private KindGroup kindGroup;

    private String kindLatin;

    private String kindPolish;

    private String kindEnglish;

    public KindGroup getKindGroup() {
        return kindGroup;
    }

    public void setKindGroup(KindGroup kindGroup) {
        this.kindGroup = kindGroup;
    }

    public String getKindLatin() {
        return kindLatin;
    }

    public void setKindLatin(String kindLatin) {
        this.kindLatin = kindLatin;
    }

    public String getKindPolish() {
        return kindPolish;
    }

    public void setKindPolish(String kindPolish) {
        this.kindPolish = kindPolish;
    }

    public String getKindEnglish() {
        return kindEnglish;
    }

    public void setKindEnglish(String kindEnglish) {
        this.kindEnglish = kindEnglish;
    }

    public Integer getKindId() {
        return kindId;
    }
}
