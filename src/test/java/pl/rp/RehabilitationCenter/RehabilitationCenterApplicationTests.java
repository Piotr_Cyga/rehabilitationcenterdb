package pl.rp.RehabilitationCenter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RehabilitationCenterApplicationTests {

	@Test
	public void contextLoads() {
	}

}
